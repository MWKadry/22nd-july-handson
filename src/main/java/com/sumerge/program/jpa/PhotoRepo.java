package com.sumerge.program.jpa;


import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;

@Stateless
public class PhotoRepo {

    private static final Logger LOGGER = Logger.getLogger(PhotoRepo.class.getName());

    @PersistenceContext
    private EntityManager em;

    public List<Photo> getAllPhotos() {
        LOGGER.info("Fetching Photos list");
        try {
            return em.createNamedQuery("Photo.findAll", Photo.class).getResultList();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void addPhoto(Photo photo) {
        LOGGER.info("Saving new address " + photo);
        try {
            em.persist(photo);
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void updatePhoto(Photo photo) {
        try {
            em.merge(photo);

        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void deletePhoto(int photoId) {

        Photo photo = em.find(Photo.class, photoId);
        if (photo == null)
            throw new IllegalArgumentException("Photo with ID: " + photoId + "does not exist in the database");

        em.remove(photo);

    }

}
