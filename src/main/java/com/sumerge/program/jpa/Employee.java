package com.sumerge.program.jpa;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name ="EMPLOYEE")
@NamedQueries({
        @NamedQuery(name = "Employee.findAll", query = "SELECT e FROM Employee e"),
        @NamedQuery(name = "Employee.findByTitle", query = "SELECT  e FROM Employee e where e.jobTitle = :jobTitle")
})

@XmlRootElement
@EntityListeners(Listener.class)
public class Employee implements Serializable {

    @Id
    @Column(name = "EMPID")
    private String empId;

    @Column(name = "DEPTCODE")
    private String deptCode;

    @Column(name = "JOBTITLE")
    private String jobTitle;

    @Column(name = "GIVENNAME")
    private String givenName;

    @Column(name = "FAMILYNAME")
    private String familyName;

    @Column(name = "COMMONNAME")
    private String commonName;

    @Column(name = "NAMETITLE")
    private String nameTitle;

    @JsonIgnore
    @ManyToMany
    @JoinTable(
            name = "PROJECTMEMBER",
            joinColumns = @JoinColumn(name = "EMPID"),
            inverseJoinColumns = @JoinColumn(name = "PROJID"))
    private List<Project> projects;

    public Employee() {
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getCommonName() {
        return commonName;
    }

    public void setCommonName(String commonName) {
        this.commonName = commonName;
    }

    public String getNameTitle() {
        return nameTitle;
    }

    public void setNameTitle(String nameTitle) {
        this.nameTitle = nameTitle;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }
}
