package com.sumerge.program.jpa;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name="DEPARTMENT")
@NamedQueries({
        @NamedQuery(name = "Department.findAll", query = "SELECT e FROM Department e")
})
@XmlRootElement
public class Department implements Serializable {

    @Id
    @Column(name = "DEPTCODE")
    private String deptCode;

    @Column(name = "DEPTNAME")
    private String deptName;

    @Column(name = "MANAGER")
    private String manager;

    public Department() {
    }

    public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }
}
