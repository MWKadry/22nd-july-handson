package com.sumerge.program.jpa;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table (name = "EMAIL")
@NamedQueries({
        @NamedQuery(name = "Email.findAll", query = "SELECT e FROM Email e")
})
@XmlRootElement
public class Email implements Serializable {


    @Id
    @Column(name = "EMAILID")
    private int emailId;


    @Column(name = "EMAILADDRESS")
    private String emailAddress;

    @Column(name = "EMAILTYPE")
    private String emailType;

    @ManyToOne
    @JoinColumn(name = "EMPID")
    private Employee employee;

    public Email() {
    }

    public int getEmailId() {
        return emailId;
    }

    public void setEmailId(int emailId) {
        this.emailId = emailId;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getEmailType() {
        return emailType;
    }

    public void setEmailType(String emailType) {
        this.emailType = emailType;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

}
