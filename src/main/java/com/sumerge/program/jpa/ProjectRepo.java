package com.sumerge.program.jpa;


import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;

@Stateless
public class ProjectRepo {

    private static final Logger LOGGER = Logger.getLogger(ProjectRepo.class.getName());

    @PersistenceContext
    private EntityManager em;

    public List<Project> getAllProjects() {
        LOGGER.info("Fetching Projects list");
        try {
            return em.createNamedQuery("Project.findAll", Project.class).getResultList();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void addProject(Project project) {
        LOGGER.info("Saving new project " + project);
        try {
            em.persist(project);
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void updateProject(Project project) {
        try {
            em.merge(project);

        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void deleteProject(String projId) {

        Project project = em.find(Project.class, projId);
        if (project == null)
            throw new IllegalArgumentException("Project with ID: " + projId + "does not exist in the database");

        em.remove(project);

    }

}
