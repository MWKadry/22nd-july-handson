package com.sumerge.program.jpa;


import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;

@Stateless
public class DepartmentRepo {

    private static final Logger LOGGER = Logger.getLogger(DepartmentRepo.class.getName());

    @PersistenceContext
    private EntityManager em;

    public List<Department> getAllDepartments() {
        LOGGER.info("Fetching Departments list");
        try {
            return em.createNamedQuery("Department.findAll", Department.class).getResultList();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void addDepartment(Department department) {
        LOGGER.info("Saving new department " + department);
        try {
            em.persist(department);
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void updateDepartment(Department department) {
        try {
            em.merge(department);

        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void deleteDepartment(String deptCode) {

        Department department = em.find(Department.class, deptCode);
        if (department == null)
            throw new IllegalArgumentException("Department with ID: " + deptCode + "does not exist in the database");

        em.remove(department);

    }

}
