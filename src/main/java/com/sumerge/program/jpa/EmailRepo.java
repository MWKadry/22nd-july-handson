package com.sumerge.program.jpa;


import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;

@Stateless
public class EmailRepo {
    private static final Logger LOGGER = Logger.getLogger(EmailRepo.class.getName());

    @PersistenceContext
    private EntityManager em;

    public List<Email> getAllEmails() {
        LOGGER.info("Fetching Emails list");
        try {
            return em.createNamedQuery("Email.findAll", Email.class).getResultList();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void addEmail(Email email) {
        LOGGER.info("Saving new email " + email);
        try {
            em.persist(email);
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void updateEmail(Email email) {
        try {
            em.merge(email);

        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void deleteEmail(int emailId) {

        Email email = em.find(Email.class, emailId);
        if (email == null)
            throw new IllegalArgumentException("Email with ID: " + emailId + "does not exist in the database");

        em.remove(email);

    }

}
