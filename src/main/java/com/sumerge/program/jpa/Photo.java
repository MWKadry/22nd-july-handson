package com.sumerge.program.jpa;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "PHOTO")
@NamedQueries({
        @NamedQuery(name = "Photo.findAll", query = "SELECT e FROM Photo e")
})
@XmlRootElement
public class Photo implements Serializable {

    @Id
    @Column(name = "PHOTOID")
    private int photoId;

    @Column(name = "IMAGENAME")
    private String imageName;

    @OneToOne
    @JoinColumn(name = "EMPID")
    private Employee employee;

    public Photo() {
    }

    public int getPhotoId() {
        return photoId;
    }

    public void setPhotoId(int photoId) {
        this.photoId = photoId;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
}
