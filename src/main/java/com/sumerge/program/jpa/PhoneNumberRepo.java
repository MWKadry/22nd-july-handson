package com.sumerge.program.jpa;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;

@Stateless
public class PhoneNumberRepo {
    private static final Logger LOGGER = Logger.getLogger(PhoneNumberRepo.class.getName());

    @PersistenceContext
    private EntityManager em;

    public List<PhoneNumber> getAllPhoneNumbers() {
        LOGGER.info("Fetching phoneNumbers list");
        try {
            return em.createNamedQuery("PhoneNumber.findAll", PhoneNumber.class).getResultList();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void addPhoneNumber(PhoneNumber phoneNumber) {
        LOGGER.info("Saving new phoneNumber " + phoneNumber);
        try {
            em.persist(phoneNumber);
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void updatePhoneNumber(PhoneNumber phoneNumber) {
        try {
            em.merge(phoneNumber);

        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void deletePhoneNumber(int phoneId) {

        PhoneNumber phoneNumber = em.find(PhoneNumber.class, phoneId);
        if (phoneNumber == null)
            throw new IllegalArgumentException("phoneNumber with ID: " + phoneId + "does not exist in the database");

        em.remove(phoneNumber);

    }



}
