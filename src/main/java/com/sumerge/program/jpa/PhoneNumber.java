package com.sumerge.program.jpa;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "PHONENUMBER")
@NamedQueries({
        @NamedQuery(name = "PhoneNumber.findAll", query = "SELECT e FROM PhoneNumber e")
})

@XmlRootElement
public class PhoneNumber implements Serializable {

@Id
@Column(name = "PHONEID")
private int phoneId;

@Column(name = "LOCALNUM")
private long localNum;

@Column(name = "INTLPREFIX")
private String  intlPrefix;

@Column(name = "PHONETYPE")
private String phoneType;



    @ManyToOne
    @JoinColumn(name = "EMPID")
    private Employee employee;

public PhoneNumber() {

}

    public int getPhoneId() {
        return phoneId;
    }

    public void setPhoneId(int phoneId) {
        this.phoneId = phoneId;
    }

    public long getLocalNum() {
        return localNum;
    }

    public void setLocalNum(long localNum) {
        this.localNum = localNum;
    }

    public String getIntlPrefix() {
        return intlPrefix;
    }

    public void setIntlPrefix(String intlPrefix) {
        this.intlPrefix = intlPrefix;
    }

    public String getPhoneType() {
        return phoneType;
    }

    public void setPhoneType(String phoneType) { this.phoneType = phoneType;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
}
