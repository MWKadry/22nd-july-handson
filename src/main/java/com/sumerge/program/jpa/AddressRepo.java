package com.sumerge.program.jpa;


import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;

@Stateless
public class AddressRepo {
    private static final Logger LOGGER = Logger.getLogger(AddressRepo.class.getName());

    @PersistenceContext
    private EntityManager em;

    public List<Address> getAllAddresses() {
        LOGGER.info("Fetching Addresses list");
        try {
            return em.createNamedQuery("Address.findAll", Address.class).getResultList();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void addAddress(Address address) {
        LOGGER.info("Saving new address " + address);
        try {
            em.persist(address);
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void updateAddress(Address address) {
        try {
            em.merge(address);

        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void deleteAddress(int addressId) {

        Address address = em.find(Address.class, addressId);
        if (address == null)
            throw new IllegalArgumentException("Address with addressId: " + addressId + "does not exist in the database");

        em.remove(addressId);

    }

}
