package com.sumerge.program.jpa;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.print.DocFlavor;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;

@Stateless
public class EmployeeRepo {

    private static final Logger LOGGER = Logger.getLogger(EmployeeRepo.class.getName());

    @PersistenceContext
    private EntityManager em;

    public List<Employee> getAllEmployees() {
        LOGGER.info("Fetching employees list");
        try {
            return em.createNamedQuery("Employee.findAll", Employee.class).getResultList();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }


    public List<Employee> getEmployeeByTitle(String jobTitle) {

        List<Employee> allTitles = em.createNamedQuery("Employee.findByTitle", Employee.class).setParameter("jobTitle",jobTitle).getResultList();

        return allTitles;
    }

    public List<Project> getAllProjects(String empId){

        List<Project> allProjects = em.createNamedQuery("Project.findAll", Project.class).getResultList();
        List<Project> relatedProjects = new ArrayList<>();

        for (Project project : allProjects) {
            List<Employee> allEmployees = project.getEmployees();

            for(Employee employee : allEmployees){

                if(empId.equals(employee.getEmpId())){
                    relatedProjects.add(project);
                }
            }
        }
        return relatedProjects;
    }

    public void addEmployee(Employee employee) {
        LOGGER.info("Saving new employee " + employee);
        try {
            em.persist(employee);
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void updateEmployee(Employee employee) {
        try {
            em.merge(employee);

        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            throw e;
        }
    }

    public void deleteEmployee(String empId) {

        Employee employee = em.find(Employee.class, empId);
        if (employee == null)
            throw new IllegalArgumentException("Employee with ID: " + empId + "does not exist in the database");

        em.remove(employee);

    }

}
