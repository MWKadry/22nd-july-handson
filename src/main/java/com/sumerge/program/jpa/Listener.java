package com.sumerge.program.jpa;

import javax.persistence.*;

public class Listener {

    @PrePersist
    public void methodInvokedBeforePersist(Employee employee) {
        System.out.println("persisting employee with id = " + employee.getEmpId());
    }

    @PostPersist
    public void methodInvokedAfterPersist(Employee employee) {
        System.out.println("Persisted employee with id = " + employee.getEmpId());
    }

    @PreUpdate
    public void methodInvokedBeforeUpdate(Employee employee) {
        System.out.println("Updating employee with id = " + employee.getEmpId());
    }

    @PostUpdate
    public void methodInvokedAfterUpdate(Employee employee) {
        System.out.println("Updated employee with id = " + employee.getEmpId());
    }

    @PreRemove
    private void methodInvokedBeforeRemove(Employee employee) {
        System.out.println("Removing employee with id = " + employee.getEmpId());
    }

    @PostRemove
    public void methodInvokedAfterRemove(Employee employee) {
        System.out.println("Removed employee with id = " + employee.getEmpId() );
    }

}
