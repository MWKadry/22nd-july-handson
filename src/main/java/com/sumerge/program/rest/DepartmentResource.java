package com.sumerge.program.rest;

import com.sumerge.program.jpa.Department;
import com.sumerge.program.jpa.DepartmentRepo;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("department")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@RequestScoped
public class DepartmentResource {

    private static final Logger LOGGER = Logger.getLogger(com.sumerge.program.rest.DepartmentResource.class.getName());


    @EJB
    private DepartmentRepo repo;

    @GET
    public Response searchDepartment() {
        try {
            return Response.ok().
                    entity(repo.getAllDepartments()).
                    build();
        } catch (Exception e) {
            return Response.serverError().
                    entity(e).
                    build();
        }
    }

    @POST
    public Response addDepartment(Department department) {
        try {
            repo.addDepartment(department);
            return Response.ok().
                    build();
        } catch (Exception e) {
            return Response.serverError().
                    entity(e.getMessage()).
                    build();
        }
    }

    @DELETE
    @Path("{id}")
    public Response deleteDepartment(@PathParam("id") String deptCode) {
        try {
            repo.deleteDepartment(deptCode);
            return Response.ok().
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }

    @PUT
    public Response updateDepartment(Department department) {
        try {
            repo.updateDepartment(department);
            return Response.ok().build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage())
                    .build();
        }
    }
}
