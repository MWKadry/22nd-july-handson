package com.sumerge.program.rest;

import com.sumerge.program.jpa.Employee;
import com.sumerge.program.jpa.EmployeeRepo;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("employee")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@RequestScoped
public class EmployeeResource {

        private static final Logger LOGGER = Logger.getLogger(com.sumerge.program.rest.EmployeeResource.class.getName());


        @EJB
        private EmployeeRepo repo;

        @GET
        public Response searchEmployee() {
            try {
                return Response.ok().
                        entity(repo.getAllEmployees()).
                        build();
            } catch (Exception e) {
                return Response.serverError().
                        entity(e).
                        build();
            }
        }


        @GET
        @Path("{id}/project")
        public Response searchProject(@PathParam("id") String  empId) {
            try {
                return Response.ok().
                    entity(repo.getAllProjects(empId)).
                            build();
            } catch (Exception e) {
                return Response.serverError().
                    entity(e).
                    build();
            }
        }

        @GET
        @Path("{id}/jobTitle")
        public Response searchByJobTitle(@PathParam("id") String  jobTitle) {
            try {
                return Response.ok().
                    entity(repo.getEmployeeByTitle(jobTitle)).
                    build();
            } catch (Exception e) {
                return Response.serverError().
                    entity(e).
                    build();
            }
        }

        @POST
        public Response addEmployee(Employee employee) {
            try {
                repo.addEmployee(employee);
                return Response.ok().
                        build();
            } catch (Exception e) {
                return Response.serverError().
                        entity(e.getMessage()).
                        build();
            }
        }

       @DELETE
       @Path("{id}")
       public Response deleteEmployee(@PathParam("id") String  empId) {
           try {
               repo.deleteEmployee(empId);
               return Response.ok().
                       build();
           } catch (Exception e) {
               LOGGER.log(SEVERE, e.getMessage(), e);
               return Response.serverError().
                       entity(e.getClass() + ": " + e.getMessage()).
                       build();
           }
       }

       @PUT
       public Response updateEmployee(Employee employee) {
            try {
                repo.updateEmployee(employee);
                return Response.ok().build();
            }
            catch (Exception e) {
                LOGGER.log(SEVERE, e.getMessage(), e);
                return Response.serverError().
                        entity(e.getClass() + ": " + e.getMessage())
                        .build();
            }
       }
    }

