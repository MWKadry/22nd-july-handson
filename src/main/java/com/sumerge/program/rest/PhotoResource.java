package com.sumerge.program.rest;

import com.sumerge.program.jpa.Photo;
import com.sumerge.program.jpa.PhotoRepo;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;


@Path("photo")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@RequestScoped
public class PhotoResource {
    private static final Logger LOGGER = Logger.getLogger(com.sumerge.program.rest.PhotoResource.class.getName());


    @EJB
    private PhotoRepo repo;

    @GET
    public Response searchPhoto() {
        try {
            return Response.ok().
                    entity(repo.getAllPhotos()).
                    build();
        } catch (Exception e) {
            return Response.serverError().
                    entity(e).
                    build();
        }
    }

    @POST
    public Response addPhoto(Photo photo) {
        try {
            repo.addPhoto(photo);
            return Response.ok().
                    build();
        } catch (Exception e) {
            return Response.serverError().
                    entity(e.getMessage()).
                    build();
        }
    }

    @DELETE
    @Path("{id}")
    public Response deletePhoto(@PathParam("id") int photoId) {
        try {
            repo.deletePhoto(photoId);
            return Response.ok().
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }

    @PUT
    public Response updatePhoto(Photo photo) {
        try {
            repo.updatePhoto(photo);
            return Response.ok().build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage())
                    .build();
        }
    }
}
