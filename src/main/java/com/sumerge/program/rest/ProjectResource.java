package com.sumerge.program.rest;


import com.sumerge.program.jpa.Employee;
import com.sumerge.program.jpa.Project;
import com.sumerge.program.jpa.ProjectRepo;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("project")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@RequestScoped
public class ProjectResource {
    private static final Logger LOGGER = Logger.getLogger(com.sumerge.program.rest.ProjectResource.class.getName());


    @EJB
    private ProjectRepo repo;

    @GET
    public Response searchProject() {
        try {
            return Response.ok().
                    entity(repo.getAllProjects()).
                    build();
        } catch (Exception e) {
            return Response.serverError().
                    entity(e).
                    build();
        }
    }


    @POST
    public Response addProject(Project project) {
        try {
            repo.addProject(project);
            return Response.ok().
                    build();
        } catch (Exception e) {
            return Response.serverError().
                    entity(e.getMessage()).
                    build();
        }
    }

    @DELETE
    @Path("{id}")
    public Response deleteProject(@PathParam("id") String projId) {
        try {
            repo.deleteProject(projId);
            return Response.ok().
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }

    @PUT
    public Response updateProject(Project project) {
        try {
            repo.updateProject(project);
            return Response.ok().build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage())
                    .build();
        }
    }

}
