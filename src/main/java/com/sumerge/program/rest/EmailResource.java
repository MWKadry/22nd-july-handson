package com.sumerge.program.rest;

import com.sumerge.program.jpa.Email;
import com.sumerge.program.jpa.EmailRepo;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("email")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@RequestScoped
public class EmailResource {

    private static final Logger LOGGER = Logger.getLogger(com.sumerge.program.rest.EmailResource.class.getName());


    @EJB
    private EmailRepo repo;

    @GET
    public Response searchEmail() {
        try {
            return Response.ok().
                    entity(repo.getAllEmails()).
                    build();
        } catch (Exception e) {
            return Response.serverError().
                    entity(e).
                    build();
        }
    }

    @POST
    public Response addEmail(Email email) {
        try {
            repo.addEmail(email);
            return Response.ok().
                    build();
        } catch (Exception e) {
            return Response.serverError().
                    entity(e.getMessage()).
                    build();
        }
    }

    @DELETE
    @Path("{id}")
    public Response deleteEmail(@PathParam("id") int emailId) {
        try {
            repo.deleteEmail(emailId);
            return Response.ok().
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }

    @PUT
    public Response updateEmail(Email email) {
        try {
            repo.updateEmail(email);
            return Response.ok().build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage())
                    .build();
        }
    }
}