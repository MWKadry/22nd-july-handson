package com.sumerge.program.rest;

import com.sumerge.program.jpa.PhoneNumber;
import com.sumerge.program.jpa.PhoneNumberRepo;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("phoneNumber")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@RequestScoped
public class PhoneNumberResource {

    private static final Logger LOGGER = Logger.getLogger(com.sumerge.program.rest.PhoneNumberResource.class.getName());


    @EJB
    private PhoneNumberRepo repo;

    @GET
    public Response searchPhoneNumber() {
        try {
            return Response.ok().
                    entity(repo.getAllPhoneNumbers()).
                    build();
        } catch (Exception e) {
            return Response.serverError().
                    entity(e).
                    build();
        }
    }

    @POST
    public Response addPhoneNumber(PhoneNumber phoneNumber) {
        try {
            repo.addPhoneNumber(phoneNumber);
            return Response.ok().
                    build();
        } catch (Exception e) {
            return Response.serverError().
                    entity(e.getMessage()).
                    build();
        }
    }

    @DELETE
    @Path("{id}")
    public Response deletePhoneNumber(@PathParam("id") int phoneId) {
        try {
            repo.deletePhoneNumber(phoneId);
            return Response.ok().
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }

    @PUT
    public Response updatePhoneNumber(PhoneNumber phoneNumber) {
        try {
            repo.updatePhoneNumber(phoneNumber);
            return Response.ok().build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage())
                    .build();
        }
    }


}
