package com.sumerge.program.rest;

import com.sumerge.program.jpa.Address;
import com.sumerge.program.jpa.AddressRepo;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("address")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@RequestScoped
public class AddressResource {

    private static final Logger LOGGER = Logger.getLogger(com.sumerge.program.rest.AddressResource.class.getName());


    @EJB
    private AddressRepo repo;

    @GET
    public Response searchAddress() {
        try {
            return Response.ok().
                    entity(repo.getAllAddresses()).
                    build();
        } catch (Exception e) {
            return Response.serverError().
                    entity(e).
                    build();
        }
    }

    @POST
    public Response addAddress(Address address) {
        try {
            repo.addAddress(address);
            return Response.ok().
                    build();
        } catch (Exception e) {
            return Response.serverError().
                    entity(e.getMessage()).
                    build();
        }
    }

    @DELETE
    @Path("{id}")
    public Response deleteAddress(@PathParam("id") int addressId) {
        try {
            repo.deleteAddress(addressId);
            return Response.ok().
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }

    @PUT
    public Response updateAddress(Address address) {
        try {
            repo.updateAddress(address);
            return Response.ok().build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage())
                    .build();
        }
    }
}